package com.example.message.conversation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/message/send")
    public List<String> sendMessage(@RequestBody MessageDTO message){
        return messageService.send(message.getSender(),message.getReceiver(),message.getText());
    }
}
