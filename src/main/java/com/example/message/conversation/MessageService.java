package com.example.message.conversation;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {

    public static final int MESSAGE_MAX_SIZE = 160;

    public static final String DEFAULT_OFFSET = "Message 1 of 1";

    public List<String> send(String from, String to, String text){

        System.out.println("Message send from " + from + " to " + to);

        List<String> messages = new ArrayList<>();

        int messageCounter = 1;
        int totalMessageEstimation = getFirstMessageEstimation(text);
        int totalMessageEstimationLengthInChars = Integer.toString(totalMessageEstimation).length();

        if(totalMessageEstimation == 1){
            System.out.println(text);
            return List.of(text);
        }

        String remainedText = text;

        while (remainedText.length() > 0){
            String customOffset = " - part " + messageCounter + " of " ;
            int chunkSize = MESSAGE_MAX_SIZE - customOffset.length() - totalMessageEstimationLengthInChars;
            if(chunkSize > remainedText.length()) {
                chunkSize = remainedText.length();
            }
            String messageChunk = remainedText.substring(0, chunkSize) + customOffset;
            messages.add(messageChunk);
            messageCounter += 1;
            remainedText = remainedText.substring(chunkSize);
        }

        if(totalMessageEstimation < messageCounter){
            totalMessageEstimation = messageCounter - 1;
        }

        List<String> messagesWithFinalEstimation = new ArrayList<>();

        int finalTotalMessageEstimation = totalMessageEstimation;
        messages.forEach(message -> {
            messagesWithFinalEstimation.add(message + finalTotalMessageEstimation);
            System.out.println(message + finalTotalMessageEstimation);
        });

        return messagesWithFinalEstimation;
    }

    private int getFirstMessageEstimation(String text){
        return text.length() / (MESSAGE_MAX_SIZE - DEFAULT_OFFSET.length()) + 1;
    }


}