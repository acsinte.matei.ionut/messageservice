package com.example.message.conversation;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageServiceTest {

    public MessageService messageService;

    public String sender = "test sender";

    public String receiver = "test receiver";

    @Test
    public void shouldDisplayOnlyTheMessageIfMessageHasLessThanOneHundredSixtyChars(){

        //given
        messageService = new MessageService();
        String shortText = "Short Text";

        //when
        List<String> messages = messageService.send(sender, receiver, shortText);

        //then
        Assert.assertEquals(1,messages.size());
        Assert.assertEquals(shortText, messages.get(0));
    }

    @Test
    public void shouldReturnMultipleMessagesIfMessageHasMoreThanOneHundredSixtyChars(){

        //given
        messageService = new MessageService();
        String normalText = readTextFromFile("src/main/resources/static/normal_test.txt");

        //when
        List<String> messages = messageService.send(sender, receiver, normalText);

        //then
        Assert.assertTrue( messages.size() > 1);
    }

    @Test
    public void shouldReturnMultipleMessagesWithExactlyOneHundredSixtyCharsEachExceptTheLastMessage(){

        //given
        messageService = new MessageService();
        String normalText = readTextFromFile("src/main/resources/static/normal_test.txt");

        //when
        List<String> messages = messageService.send(sender, receiver, normalText);
        messages.remove(messages.size() - 1);

        //then
        messages.forEach(message -> Assert.assertEquals(160, message.length()));
    }

    @Test
    public void shouldHavaNoMoreMessagesThanPrecisedInEstimationForHugeInputs(){
        //given
        messageService = new MessageService();
        String normalText = readTextFromFile("src/main/resources/static/huge_test.txt");

        //when
        List<String> messages = messageService.send(sender, receiver, normalText);
        int totalMessages = messages.size();
        String[] parsedMessages = messages.get(0).split(" ");
        int expectedMessages = Integer.parseInt(parsedMessages[parsedMessages.length - 1]);

        //then
        Assert.assertEquals(expectedMessages, totalMessages);
    }

    private String readTextFromFile(String fileName)
    {
        List<String> lines = Collections.emptyList();
        try
        {
            lines= Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return lines.stream().reduce("", (acc, line) -> acc + line );
    }
}
